    !
SUBROUTINE IOMNGR  ( INT )

    use modtapes, only: jobname,iinput,ioutput,IBLANK
    use modoptim, only: optimFlag



    IMPLICIT none

    integer :: INT,I

    CHARACTER*80  FILECONTROL, FILEREPORT, FILEOPTIM, FILEEXPDISPVOLU, FILEEXPPRESDROP, FILEOUT1, FILEOUT2, FILEOUT3
    
    DO i = 1, size(iinput)
        iinput(i) = i
    ENDDO
    DO i = 1, size(ioutput)
        ioutput(i) = i + size(iinput)
    ENDDO

    !iinput(1)   = 1
    !iinput(2)   = 2
    !iinput(3)   = 3
    !ioutput(1)  = 11

    IF ( INT .EQ. 0 ) THEN

        open (iinput(1), file = "jobname.txt")

        read (iinput(1),'(a)') jobname

        IBLANK = 0
        DO I = 1,80
            IF (jobname(I:I).EQ.' ') THEN
                IBLANK=I
                GO TO 10
            ENDIF
        ENDDO
10      CONTINUE

        close (unit = iinput(1))

        WRITE (FILECONTROL,'(A)') jobname(1:IBLANK-1)//'_CONTROL.txt'
        WRITE (FILEREPORT,'(A)') jobname(1:IBLANK-1)//'_REPORT.txt'
        WRITE (FILEOPTIM,'(A)') jobname(1:IBLANK-1)//'_OPTIM.txt'
        
        WRITE (FILEOUT1,'(A)') jobname(1:IBLANK-1)//'_dispvolu.out'
        WRITE (FILEOUT2,'(A)') jobname(1:IBLANK-1)//'_presdrop.out'
        
        open (unit = iinput(2)   , file = FILECONTROL)
        open (unit = ioutput(1)  , file = FILEREPORT)
        
        open (unit = ioutput(2) , file = FILEOUT1)
        open (unit = ioutput(3) , file = FILEOUT2)
        
        INQUIRE(file = FILEOPTIM, exist = optimFlag)
        
        IF (optimFlag) THEN
            WRITE (FILEEXPDISPVOLU,'(A)') jobname(1:IBLANK-1)//'_DISPVOLU.txt'
            WRITE (FILEEXPPRESDROP,'(A)') jobname(1:IBLANK-1)//'_PRESDROP.txt'
            
            WRITE (FILEOUT3,'(A)') jobname(1:IBLANK-1)//'_optim.out'
            
            open (unit=iinput(3), file = FILEOPTIM)
            open (unit=iinput(4), file = FILEEXPDISPVOLU)
            open (unit=iinput(5), file = FILEEXPPRESDROP)
            
            open (unit = ioutput(4) , file = FILEOUT3)
        ENDIF

        WRITE (*,100)
        WRITE (*,101) jobname
        WRITE (*,102)
        WRITE (*,103)


100     FORMAT (/,  '    ... READING JOBNAME FROM FILE (JOBNAME.TXT) ' )
101     FORMAT (/,  '    ... JOBNAME =  ', (a17),/ )
102     FORMAT (/,  '    ... INPUT   : CONTROL DATA  ** ( JOBNAME_CONTROL.txt)')
103     FORMAT (/,  '    ... OUTPUT  : REPORT  DATA  ** (  JOBNAME_REPORT.txt)',/)




    ELSE

        DO i = 1, size(iinput)
            close(iinput(i))
        ENDDO
        DO i = 1, size(ioutput)
            close(ioutput(i))
        ENDDO

    ENDIF


    RETURN
END
