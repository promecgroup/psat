!  **************** BEGIN MODVAR ****************
module modvar

implicit none
        
        integer ::  nd           ,  nnoel        ,  numnp        , &
                    nume         ,  nummat       ,  ngl          , & 
                    neq          ,  neqp = 0     ,  ncprop = 15  , &
                    nwk          ,  NFLAG        ,  ikr, nfiles  , &
                    np_sat=2000              
        
        real*8 :: Xlength,diam,area,xKref,poro, &
                  rho_w,visco_w, rho_o,visco_o,&
                  c1w, xkrw_max  , c1o, xkro_max , &
                  Qi  , Si_inj , Sr_desloc, &
                  dt , timef
        
        real*8  ::  value_mem   
        
        character*80 	hed
        character*14 	etype
           
 end module modvar 
!  **************** END MODVAR ******************


!  **************** BEGIN MODTAPES **************
module modtapes

implicit none
      
         integer ::   iinput(10)  , ioutput(10), IBLANK, IPLT, IPLOT
                     
                     
         character*80 jobname
                    
end module modtapes
!  **************** END MODTAPES ****************


!  **************** BEGIN MODMESH  **************

module modmesh

            implicit none

            integer, allocatable :: &
                     id  (:,:),  lm(:,:),  incid (:,:), mtype (:)
                     
            real*8, allocatable ::  &
                     x (:),  y (:),  z (:), prop (:,:)
         
end module modmesh 
!  **************** END MODMESH  ****************


!  **************** BEGIN MODLOADS **************
module modloads

   implicit none

   real*8, allocatable ::  &
                     Qv(:)
       
end module modloads 
!  **************** END MODLOADS ****************


!  **************** BEGIN MODSOLVER *************
module modsolver
   implicit none
            
            
   real*8, allocatable  :: &
                 stiff(:),   press(:), Sat(:), fp(:)
                                
   integer, allocatable :: &                               
                  maxa(:),  MHT(:)
   
   real*8, allocatable  :: frac_flow( : ), df( : ), xkrw(:), xkro(:)

                                
end module modsolver
!  **************** END MODSOLVER ***************

!  **************** BEGIN MODOPTIM *************
module modoptim
    implicit none
             
    !INPUTS         
    real*8, allocatable  :: &
        dispVolu_exp(:,:), &
        presDrop_exp(:,:)
    
    integer :: &
        maxIter, &
        derType
    
    logical :: &
        optimFlag
    
    real*8 :: &
        phiTol, &
        deltaAlpha, &
        niInic, &
        niMax
    
    !MIDVARS
    real*8, allocatable  :: &
        dispVolu_num(:,:), &
        presDrop_num(:,:), &
        gradient(:), &
        hessian(:,:), &
        VetorResiduo(:), &
        VecWeight(:)
    
    integer ::  &
        nxpDispVolu, &
        nxpPresDrop, &
        nflagDV, &
        nflagPD
                         
end module modoptim
!  **************** END MODOPTIM ***************