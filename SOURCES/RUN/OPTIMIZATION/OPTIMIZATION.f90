SUBROUTINE OPTIMIZATION

    use modoptim
    use modvar, only: c1w, c1o
    use modtapes, only: ioutput
    
    implicit none
    
    logical ::  flagSucesso, flagAux
    logical ::  flagInLoop
    logical ::  criterio1, criterio2, criterio3
    real*8  ::  criPhi, criAlp, criGra
    
    real*8  ::  ni
    real*8  ::  phi, phiNovo
    integer ::  numPar, nxpSize, iter, numRes, numAtiv, numNovo
    
    real*8, allocatable ::  alpha0(:), alpha(:), delta(:), alpTemp(:)
    real*8, allocatable ::  hessModif(:,:)
    integer,allocatable ::  ativ(:), ativNovo(:)
    
    real*8  ::  epsilon0(nxpDispVolu + nxpPresDrop), VecRes(nxpDispVolu + nxpPresDrop)
    
    integer ::  daCount
    character*80    ::  fmt
    
    numAtiv = 0 !alguma coisa(?) encontra-se sobre uma alguma restricao
    numPar = 2 !Variaveis de permeabilidade, excluindo valMax
    numRes = numPar + 2 !Variaveis de permeabilidade, incluindo valMax (injetado e deslocado)
    
    write(fmt,*) numPar
    
    fmt = "(i3.3,x,es12.5,"//trim(adjustl(fmt))//"(x,es12.5))"
    
    daCount = 0
    
    iter = 0
    nxpSize = nxpDispVolu+nxpPresDrop
    
    ALLOCATE(alpha0(numPar), alpha(numPar), gradient(numPar), delta(numPar), alpTemp(numPar))
    ALLOCATE(ativ(numRes), ativNovo(numRes))
    ALLOCATE(hessian(numPar,numPar), hessModif(numPar,numPar))
    ALLOCATE(VecWeight(nxpSize))
    
    ni = niInic
    alpha0(1) = c1w
    alpha0(2) = c1o

    alpha(:) = alpha0(:)    
    
    CALL VetorPeso(nxpDispVolu, nxpPresDrop, dispVolu_exp, presDrop_exp, VecWeight)
    CALL CalcPhi(nxpDispVolu, nxpPresDrop, dispVolu_num, dispVolu_exp, presDrop_num, presDrop_exp, VecWeight, phi, epsilon0)
    
    write(*,*) "------------------------------------------------------------"
    write(*,*)
    write(*,*) "Optimizing Alpha"
    write(*,*) "Initial Phi =", phi
    write(*,*) "alpha =", alpha
    
    write(ioutput(4),fmt) iter, phi, alpha(1), alpha(2)
    
    DO while (.not. flagSucesso)
        
        CALL CalcHessJac(derType, numPar, nxpSize, deltaAlpha, epsilon0, alpha, hessian, gradient)
        
        DO while (ni.le.niMax)
            CALL ModHess(numPar, ni, hessian, hessModif)
            ativNovo(:) = ativ(:)
            numNovo = numAtiv
            CALL NovoPasso(numPar, gradient, hessModif, delta) !alpha*delta = -gradient (Solve for delta)
            
            alpTemp(:) = alpha(:) + delta(:)
            CALL SOLVE_OPTIM(numPar, alpTemp, phiNovo, VecRes)
            write(*,*) "Temp Phi =", phiNovo
            IF (phiNovo < phi) THEN
                flagInLoop = .true.
                write(*,*) "break"
                EXIT
            ENDIF
            
            ni = ni*10
            flagInLoop = .false.
        ENDDO
        
        alpha(:) = alpha(:) + delta(:)
        IF (ni>niInic) ni = ni/100.d0
        
        criterio1 = .false.
        criterio2 = .false.
        criterio3 = .false.
        
        criPhi = (phi - phiNovo)/(phiTol * (1.d0 + dabs(phiNovo)))
        criterio1 = criPhi < 1.d0
        
        criAlp = norm2(delta)/(dsqrt(phiTol) * (1.d0 + norm2(alpha)))
        criterio2 = criAlp < 1.d0
        
        criGra = norm2(gradient)/(sqrt(real(numPar)) * (phiTol**0.2d0) * (1.d0 + dabs(phiNovo)))
        criterio3 = criGra < 1
        
        flagAux = (criterio1 .and. criterio2 .and. (.not. criterio3) .and. flagInLoop)
        flagSucesso = (criterio1 .and. criterio2 .and. criterio3 .and. flagInLoop)
        
        IF (flagAux) THEN
            daCount = daCount + 1
            deltaAlpha = deltaAlpha*0.1d0
        ELSE
            daCount = 0
        ENDIF
        
        CALL SOLVE_OPTIM(numPar, alpha, phiNovo, VecRes)
        
        iter = iter + 1
        phi = phiNovo
        epsilon0(:) = VecRes(:)       
        
        write(*,*)
        write(*,*) "------------------------------------------------------------"
        write(*,*)
        write(*,*) "Iteration =", iter
        write(*,*) "Current Phi =", phi
        write(*,*) "Current Alpha =", alpha
        
        write(ioutput(4),fmt) iter, phi, alpha
        
    ENDDO

ENDSUBROUTINE
    
