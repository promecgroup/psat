SUBROUTINE SOLVE_OPTIM(numPar,alpha,phi,VecRes)
    
    use modoptim
    use modvar
    
    implicit none
    
    integer,intent(in)  ::  numPar
    real*8, intent(in)  ::  alpha(numPar)
    
    real*8, intent(out) ::  phi, VecRes(nxpDispVolu + nxpPresDrop)
    
    c1w = alpha(1)
    c1o = alpha(2)
    
    write(*,*) "Tentando com alpha =", alpha
    
    CALL SOLVER
    CALL CalcPhi(nxpDispVolu, nxpPresDrop, dispVolu_num, dispVolu_exp, presDrop_num, presDrop_exp, VecWeight, phi, VecRes)

ENDSUBROUTINE