SUBROUTINE CALCHESSJAC(derType, numPar, numVal, deltaAlpha, epsilon0, alpha, hessian, gradient)
    
    use modvar, only: c1w, c1o
    
    implicit none
    
    !derType: 1=Montante, 2=Jusante, 3=Centrais
    !numVal: numero de resultados
    !numPar: numero de parametros da funcao sendo otimizada
    
    integer,    intent(in)  ::  derType, numPar, numVal
    real*8,     intent(in)  ::  deltaAlpha, alpha(numPar), epsilon0(numVal)
    
    real*8,     intent(out) ::  hessian(numPar,numPar), gradient(numPar)
    
    real*8  ::  jacob(numVal,numPar),           &
                jacobT(numPar,numVal),          &
                alphaAtras(numPar, numPar),     &
                alphaAdiante(numPar, numPar),   &
                epsilon1(numVal,numPar),        &
                epsilon2(numVal,numPar),        &
                epsTemp(numVal),                &
                alpTemp(numPar)
    
    real*8  ::  phiTemp, hessDet
    integer ::  i, j
    
    character*80    ::  fmt
    
    write(fmt,*) numPar
    fmt = "(a,i3.3,a,"//trim(adjustl(fmt))//"(x,es12.5))"
    
    epsilon1(:,:) = 0.d0
    epsilon2(:,:) = 0.d0
    
    SELECT CASE(derType)
        CASE (1) !Diferencas a montante
            DO i = 1, numPar
                DO j = 1, numPar
                    alphaAtras(i,j) = alpha(j)  !alphaAtras.Row(i) = alpha
                ENDDO
            ENDDO
            DO i = 1, numPar
                alphaAtras(i,i) = alphaAtras(i,i) - deltaAlpha
            ENDDO
            
            
            DO i = 1, numPar
                DO j = 1, numPar
                    alpTemp(j) = alphaAtras(i,j) !alpTemp = alphaAtras.Row(i)
                ENDDO
                
                CALL SOLVE_OPTIM(numPar, alpTemp, phiTemp, epsTemp) !com alpTemp no lugar de alpha (retornar VetorResiduo como epsTemp)
                
                DO j = 1, numVal
                    epsilon1(j,i) = epsTemp(j)  !epsilon1.Col(i) = epsTemp
                    epsilon2(j,i) = epsilon0(j) !epsilon2.Col(i) = epsilon0
                ENDDO
            ENDDO
                
        CASE (2) !Diferencas a jusante
            DO i = 1, numPar
                DO j = 1, numPar
                    alphaAdiante(i,j) = alpha(j)    !alphaAdiante.Row(i) = alpha
                ENDDO
            ENDDO
            DO i = 1, numPar
                alphaAdiante(i,i) = alphaAdiante(i,i) + deltaAlpha
            ENDDO
            
            
            DO i = 1, numPar
                DO j = 1, numPar
                    alpTemp(j) = alphaAdiante(i,j) !alpTemp = alphaAdiante.Row(i)
                ENDDO
                
                CALL SOLVE_OPTIM(numPar, alpTemp, phiTemp, epsTemp)
                
                DO j = 1, numVal
                    epsilon1(j,i) = epsilon0(j) !epsilon1.Col(i) = epsilon0
                    epsilon2(j,i) = epsTemp(j)  !epsilon2.Col(i) = epsTemp
                ENDDO
            ENDDO
                
        CASE (3) !Diferencas centrais
            DO i = 1, numPar
                DO j = 1, numPar
                    alphaAtras(i,j) = alpha(j)   !alphaAtras.Row(i) = alpha
                    alphaAdiante(i,j) = alpha(j) !alphaAdiante.Row(i) = alpha
                ENDDO
            ENDDO
            DO i = 1, numPar
                alphaAtras(i,i) = alphaAtras(i,i) - deltaAlpha*0.5d0
                alphaAdiante(i,i) = alphaAdiante(i,i) + deltaAlpha*0.5d0
            ENDDO
            
            DO i = 1, numPar
                DO j = 1, numPar
                    alpTemp(j) = alphaAtras(i,j) !alpTemp = alphaAtras.Row(i)
                ENDDO
                
                CALL SOLVE_OPTIM(numPar, alpTemp, phiTemp, epsTemp)
                
                DO j = 1, numVal
                    epsilon1(j,i) = epsTemp(j)  !epsilon1.Col(i) = epsTemp
                ENDDO
            ENDDO
            DO i = 1, numPar
                DO j = 1, numPar
                    alpTemp(j) = alphaAdiante(i,j) !alpTemp = alphaAdiante.Row(i)
                ENDDO
                
                CALL SOLVE_OPTIM(numPar, alpTemp, phiTemp, epsTemp)
                
                DO j = 1, numVal
                    epsilon2(j,i) = epsTemp(j)  !epsilon2.Col(i) = epsTemp
                ENDDO
            ENDDO
            
        CASE DEFAULT
            !throw error            
    ENDSELECT
    
    jacob = (epsilon2 - epsilon1)
    jacob = (1.d0/deltaAlpha) * jacob
    jacobT = Transpose(jacob)
    gradient = matmul(jacobT,epsilon0)
    hessian = matmul(jacobT,jacob)
    
    !hessDet = hessian(1,1)*hessian(2,2) - hessian(1,2)*hessian(2,1)
    
    DO i = 1, numPar 
        write(*,fmt) "Hessian[", i, "] = ", hessian(i,:) 
    ENDDO 
    
    !write(*,*) "Hessian[1] = ", hessian(1,:)
    !write(*,*) "Hessian[2] = ", hessian(2,:)
    !write(*,*) "Hessian Det = ", hessDet
    write(*,*)
    
    
ENDSUBROUTINE
    
SUBROUTINE ModHess(numPar, ni, hessian, hessModif)

    implicit none
    
    integer, intent(in) ::  numPar
    real*8, intent(in)  ::  ni, hessian(numPar,numPar)
    
    real*8, intent(out) ::  hessModif(numPar,numPar)
    
    integer ::  i
    
    hessModif(:,:) = hessian(:,:)
    
    DO i = 1, numPar
        hessModif(i,i) = hessModif(i,i) * (1 + ni)
    ENDDO
    
    !write(*,*) "hessModif[1] = ", hessModif(1,:)
    !write(*,*) "hessModif[2] = ", hessModif(2,:)
    !write(*,*)

ENDSUBROUTINE