SUBROUTINE CalcPhi(nxpDispVolu, nxpPresDrop, dispVolu_num, dispVolu_exp, presDrop_num, presDrop_exp, vecW, phi, VetorResiduo)

    implicit none
    
    integer, intent(in) ::  nxpDispVolu, nxpPresDrop
    real*8, intent(in)  ::  dispVolu_num(2,nxpDispVolu), &
                            dispVolu_exp(2,nxpDispVolu), &
                            presDrop_num(2,nxpPresDrop), &
                            presDrop_exp(2,nxpPresDrop), &
                            vecW(nxpDispVolu + nxpPresDrop)
                
    real*8, intent(out) ::  phi, VetorResiduo(nxpDispVolu + nxpPresDrop)
    
    integer ::  i
    real*8  ::  sqSum
    
    sqSum = 0.d0
    
    DO i = 1, nxpPresDrop
        VetorResiduo(i) = (presDrop_num(2,i) - presDrop_exp(2,i)) * vecW(i)
    ENDDO
    DO i = 1, nxpDispVolu
        VetorResiduo(i + nxpPresDrop) = (dispVolu_num(2,i) - dispVolu_exp(2,i)) * vecW(i + nxpPresDrop)
    ENDDO
    
    DO i = 1, nxpDispVolu + nxpPresDrop
        sqSum = sqSum + VetorResiduo(i)**2
    ENDDO
    
    phi = sqSum

ENDSUBROUTINE
    
SUBROUTINE VetorPeso(nxpDispVolu, nxpPresDrop, dispVolu_exp, presDrop_exp, VecWeight)

    !entrada experimentais
    
    implicit none
    
    integer,intent(in)  ::  nxpDispVolu, nxpPresDrop
    real*8, intent(in)  ::  dispVolu_exp(2,nxpDispVolu), presDrop_exp(2,nxpPresDrop)
    
    real*8, intent(out) :: VecWeight(nxpDispVolu + nxpPresDrop)
    
    real*8  ::  dispAux(nxpDispVolu), presAux(nxpPresDrop)
    real*8  ::  paramDV, paramPD
    integer ::  i
    
    dispAux(:) = dispVolu_exp(2,:)
    presAux(:) = presDrop_exp(2,:)
    
    paramDV = maxval(dispAux)
    paramPD = maxval(presAux)
    
    DO i = 1, nxpPresDrop
        VecWeight(i) = 1.d0/paramPD
    ENDDO
    DO i = 1, nxpDispVolu
        VecWeight(i + nxpPresDrop) = 1.d0/paramDV
    ENDDO


ENDSUBROUTINE