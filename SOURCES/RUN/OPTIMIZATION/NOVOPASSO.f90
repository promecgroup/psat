SUBROUTINE NovoPasso(numPar, gradient, hess, delta)

    implicit none
    
    integer,intent(in)  ::  numPar
    real*8, intent(in)  ::  gradient(numPar), hess(numPar,numPar)
    
    real*8, intent(out) ::  delta(numPar)
    
    real*8  ::  beta, hessDetInv
    integer ::  p, np
    
    real*8  ::  gradN(numPar), gradL(numPar), hessInv(numPar,numPar)
    
    gradN(:) = -gradient(:)
    
    !p = numAtiv
    
    !Solve for delta in [hess*delta = gradN]
    
    hessDetInv = 1.d0/(hess(1,1)*hess(2,2) - hess(1,2)*hess(2,1))
    
    hessInv(1,1) =  hessDetInv * hess(2,2)
    hessInv(1,2) = -hessDetInv * hess(1,2)
    hessInv(2,1) = -hessDetInv * hess(2,1)
    hessInv(2,2) =  hessDetInv * hess(1,1)
    
    delta = matmul(hessInv,gradN)
    
    !check
    
    gradL(:) = gradient
    beta = 1.d0
    
    !update beta
    
    delta(:) = beta*delta(:)
    
    !DO i=1, np [ativ(p + i) =  nativ(i)]
    !numAtiv = p + np
    
    write(*,*) "gradiente = ", gradient
    write(*,*) "gn = ", gradN
    write(*,*) "delta = ", delta
    write(*,*)

ENDSUBROUTINE