      SUBROUTINE SOLVER2
	
      use    modvar,    only: nwk,neq,numnp,nume,nnoel,nd,area,poro, Xlength,np_sat
      use    modvar,    only: xkref,visco_w,visco_o,c1w,c1o,Qi
      use    modvar,    only: xkrw_max,xkro_max,Si_inj,Sr_desloc,timef,dt,NFLAG
      use    modmesh,   only: x,incid,lm,id
      use    modsolver, only: maxa,stiff,Sat,fp,press,frac_flow,df,xkrw,xkro
      use    modtapes,  only: IOUTPUT
	
      IMPLICIT NONE
      
      integer :: i, ino, nsteps, istep, iel, k=0, npi, nume_max, ifac,ifac_nume
      real*8  :: time, xf, x_iel, df_iel,Sat_BL, Sat_iel=0.d0,delta_sat,df_max,xsat
      real*8  :: VP_oil,Sat_oil,Qw,Sw,sat_avg,dx,sat_avg2,Voil_preso,time_bt,Vp_oil1
      real*8  :: sat_trial, qt, Sw_iel, Fwp, xkro_p, xlambw
      
      
      
      
      
      
      ! Begin: Inicializando vari�veis:
      
      Sat(1) = 1.0-Sr_desloc
      do iel= 2, nume 
          Sat(iel) = Si_inj ! Satura��o Inicial na Amostra
      enddo

      ! End: Inicializando vari�veis:
      
      
      
      ! Begin: Avalia Kre (COREY), FracFlow e DF

      call Fracflow (frac_flow,df,xkrw,xkro,Sat_BL,visco_w,visco_o,xkrw_max,xkro_max,Si_inj,Sr_desloc,c1w,c1o,np_sat)

      ! End: Avalia Kre (COREY), FracFlow e DF
     
      
      
      
      
      DO npi = 2, np_sat
                  
            Sat_iel = Sat_iel + 1.d0/np_sat
            
            if ( Sat_iel .gt. Sat_BL) then
            
                df_max = df (npi-1)
         
                goto 121
                
            endif
                    
      ENDDO
        
121   continue

      time = 0.d0
      nsteps = timef/dt

      Sat_iel = 0.d0
      delta_sat = 1.d0/np_sat
      VP_oil = 0.d0
      sat_avg = 0.d0
      VP_oil = 0.d0
      
      
      DO istep = 1 , nsteps ! Loop no Tempo
          
          
              time = time + dt
              
              
              ! Begin: Avalia Xf (posi��o da frente de satura��o, tempo (t)
              
              Xf =  df_max * Qi*time/(area*poro) 
                     
              ! End: Avalia Xf (posi��o da frente  de satura��o, tempo (t)
              
              
              
              
              ! Begin: Avalia Campo de Satura��o, tempo (t)
              
              nume_max =  (Xf/Xlength)*nume
              ifac = ( (1.d0-Sr_desloc) - Sat_BL ) / (1.d0/np_sat) ! = 30 pontos
              
              
              
              IF (nume_max.gt.nume) nume_max = nume
              
              DO iel = 2, nume_max
                  
                
                  x_iel = ( x(incid(iel,1)) + x(incid(iel,2) ) ) * 0.5
              
                  ifac_nume = ifac/nume_max  
                  
                  df_iel = x_iel*area*poro/(Qi*time)
                  
                  do npi= Sat_BL*np_sat, (1.d0-Sr_desloc)*np_sat
                      
                      if ( df(npi) .le. df_iel ) then
                          
                          xsat = (df_iel - df(npi) ) * delta_sat /  df(npi-1) ! Interpola��o linear
                          
                          Sat(iel) = (npi-1)*(delta_sat) + (delta_sat - xsat) 
                          goto 131
                          
                      endif
                      
                  enddo
                  
              131 continue             
                  
                  
              
              ENDDO
              ! End: Avalia Campo de Satura��o, tempo (t)        
              
              
              
              
              ! Begin: Avalia Campo de Press�o, tempo (t)
              
              Press(numnp-1) = 0.0 !0.22d0/1e5 ! Danger Corrigir Condi��o Incial: P( 0 < x < L , t=0 )
              
              DO ino = numnp,2,-1
                  
                  iel = (ino - 1)
                  Sw_iel = Sat(iel)
                  

                  sat_trial = 0.d0
                  do npi = 1, np_sat
                      
                      sat_trial = sat_trial + delta_sat
                      
                      if(sat_trial.ge.Sw_iel) then
                         Fwp = frac_flow(npi)
                         xkro_p = xkro(npi)
                         goto 111
                         if( (sat_trial-Sw_iel) .gt. (delta_sat*0.05) )  write(*,*) 'Erro1!' 
                      endif
                      
                      if (npi.eq.np_sat) write(*,*) 'Erro2!'    
                  enddo
              111 continue                  
                  
                  
                  qt = Qi/Area
                  dx =  x(incid(iel,2)) - x(incid(iel,1) )                   

                  Press(ino-2) = Press(ino-1) + dx*( qt*(1.d0-Fwp)*visco_o/(xkro_p*Area) )
                  
              
              ENDDO
              
              ! PVC Press�o **********************
              Fp(:)    =  0.d0
              Fp(1)    = Qi   
              stiff(:) =  0.d0
              
              CALL Stiffp_bar1d (stiff,sat,maxa,x,area,xkref,visco_w,visco_o,xkrw,xkro,incid,lm,numnp,nume,nnoel,nd,neq,nwk,np_sat)
              CALL COLSOL       (stiff,Fp,MAXA,neq,NWK,neq+1,1,IOUTPUT(1))
              CALL COLSOL       (stiff,Fp,MAXA,neq,NWK,neq+1,2,IOUTPUT(1))
              
              Press (:) = Fp(:) 
              
              ! PVC Press�o **********************
              
              
              
              ! End: Avalia Campo de Press�o, tempo (t)
              
              
              
              
              ! Begin: Calculando Volume Produzido
              
              
              if (Sat(nume).le.Si_inj) then  ! [ Tempo < Tempo de breakthrough ]
              
                sat_avg2 = Si_inj + Qi*time/(Xf*poro*Area) ! Satura��o m�dia ( Pr� - breakthrough )
                  
                 VP_oil1 = xf*area*poro*(sat_avg2 -  Si_inj)
                 
                 write(152,*)time,Vp_oil1
                 
                 time_bt = time ! tempo de breakthrough
                 
                 
              else                           ! [ Tempo > Tempo de breakthrough ]
                  
                  
                 sat_avg = 0.d0 
                 
                 do iel=1, nume
                     dx =   x(incid(iel,2)) - x(incid(iel,1) )                   
                     sat_avg= sat_avg + sat(iel)*dx/Xlength    ! Satura��o m�dia ( Pr� - breakthrough )
                 enddo
                  
                 VP_oil =   Xlength*Area*poro*(sat_avg - Si_inj )  
              
                 write(152,*)time,Vp_oil
              
              endif
              ! End: Calculando Volume Produzido         
              
              
              
              
              ! Begin: Imprimindo resultados
              
              write(151,'(5f10.5)')time,sat(1),sat(nume),1.d0-sat(1),1.d0-sat(nume)
              
              sat_avg = 0.d0 
              do iel=1, nume
                dx =   x(incid(iel,2)) - x(incid(iel,1) )                   
                sat_avg= sat_avg + sat(iel)*dx/Xlength
              enddo
              
              
              sat_avg2 = Si_inj + Qi*time/(Xf*poro*area)
              write(150,*) time, sat_avg , sat_avg2
              
              write(79,*) time, press(1) , press(neq/2)
              
              
              IF (MOD(istep,nflag) .EQ. 0)  THEN
               
                   k=k+1
                    
                   do iel=1, nume
                       x_iel = ( x(incid(iel,1)) + x(incid(iel,2) ) ) * 0.5
              
                       write(200+k,*) x(incid(iel,1)), sat(iel), press(iel) 
              
                   enddo
                   
              ENDIF
              
              ! End: Imprimindo resultados
         
         
         
      ENDDO ! FIM Lop no Tempo
      

      write(*,*)          '   ... Breakthrough Time =', time_bt/3600, '(h)'
      write(IOUTPUT(1),*) '... Breakthrough Time =', time_bt/3600, '(h)', time_bt, '(s)'
      
      RETURN
    END SUBROUTINE SOLVER2
    
    
    

    