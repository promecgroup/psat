SUBROUTINE INPUTS
    
    use modoptim, only: optimFlag
    
    CALL control
    
    IF (optimFlag) THEN
        CALL optimControl
        CALL experiments
    ENDIF
    
    
ENDSUBROUTINE