SUBROUTINE EXPERIMENTS

    use modtapes
    use modoptim
    use modvar, only: timef, dt
    
    implicit none
    
    integer ::  CountLines
    
    nxpDispVolu = CountLines(iinput(4))
    nxpPresDrop = CountLines(iinput(5))
    
    nflagDV = int(timef/dt/nxpDispVolu)
    nflagPD = int(timef/dt/nxpPresDrop)
    
    ALLOCATE (dispVolu_num(2,nxpDispVolu), dispVolu_exp(2,nxpDispVolu))
    ALLOCATE (presDrop_num(2,nxpPresDrop), presDrop_exp(2,nxpPresDrop))
    ALLOCATE (VetorResiduo(nxpDispVolu + nxpPresDrop))
    
    CALL ReadExpData(iinput(4), nxpDispVolu, dispVolu_exp)
    CALL ReadExpData(iinput(5), nxpPresDrop, presDrop_exp)

ENDSUBROUTINE
    
SUBROUTINE ReadExpData (unit, nlines, data_vec)

    implicit none
    
    integer ::  unit, nlines
    real*8  ::  data_vec(2,nlines)
    
    integer ::  i
    
    DO i = 1, nlines
        READ(unit,*) data_vec(1,i), data_vec(2,i)
    ENDDO

ENDSUBROUTINE