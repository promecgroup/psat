!    -------------------------------------------------------------------
      SUBROUTINE CONTROL
!    -------------------------------------------------------------------
!
!     CHAMADA POR : PROGRAMA PRINCIPAL
!     RETORNA     : INFORMACOES DE CONTROLE E DADOS GERAIS
!
      use    modtapes, only : iinput , ioutput
      use    modvar,   only : NUMNP,NNOEL,NUMMAT,NGL,ND,HED, &
                              Xlength,diam,area,xKref,poro, &
                              rho_w,visco_w, rho_o,visco_o,&
                              ikr , c1w, xkrw_max  , c1o, xkro_max , &
                              Qi  , Si_inj , Sr_desloc, &
                              nume  , dt , timef  , NFLAG, nfiles
      
      IMPLICIT NONE
	
      
      CALL copyright(ioutput(1))



      READ (iinput(2),'(a)')
      READ (iinput(2),'(a)') HED
      READ (iinput(2),'(a)')
      

      READ (iinput(2),'(a)')
      READ (iinput(2),*) Xlength,diam,xKref,poro        !comprimento, diametro, permeabilidade KRef, porosidade
      READ (iinput(2),'(a)')

      READ (iinput(2),'(a)')
      READ (iinput(2),*) rho_w,visco_w, rho_o,visco_o
      READ (iinput(2),'(a)')
      
      READ (iinput(2),'(a)')
      READ (iinput(2),*) ikr , c1w, xkrw_max  , c1o, xkro_max   !kt type (Corey=1), a0_w, Krw_max, a0_o, Kro_max
      READ (iinput(2),'(a)')

      READ (iinput(2),'(a)')
      READ (iinput(2),*) Qi  , Si_inj , Sr_desloc
      READ (iinput(2),'(a)')

      READ (iinput(2),'(a)')
      READ (iinput(2),*) nume  , dt , timef  , nfiles
      
      
      
      NNOEL  = 2
      NGL    = 1
      NUMNP  = NUME+1
      ND     = NNOEL * NGL
      NUMMAT =1 
      
      
      NFLAG  = int(timef/dt/nfiles)
      
      WRITE (ioutput(1),100) NUMNP, NUME, NNOEL,NGL,NGL
      
      
      
      WRITE (ioutput(1),200)
      WRITE (ioutput(1),201) Xlength,diam,xKref,poro
      WRITE (ioutput(1),202) rho_w,visco_w, rho_o,visco_o
      WRITE (ioutput(1),203) ikr , c1w, xkrw_max  , c1o, xkro_max
      WRITE (ioutput(1),204) Qi  , Si_inj , Sr_desloc
      WRITE (ioutput(1),205) Nume  , dt , timef  , nfiles

      
      Area = 0.25*3.1416*diam**2
      
      

100   FORMAT ( ////,&
      '                       DADOS  DE CONTROLE                   ',///,  &
      ' NUMERO DE PONTOS NODAIS . . . . . . . . . . . . (NUMNP)  = ',I8//, &
      ' NUMERO DE ELEMENTOS . . . . . . . . . . . . . . (NUME)   = ',I8//, &
      ' NUMERO DE NOS POR ELEMENTO  . . . . . . . . . . (NNOEL)  = ',I5//, &      
      ' NUMERO DE GRAUS DE LIBERDADE POR NO (PRESSAO) . (NGL_P)  = ',I5//, &  
      ' NUMERO DE GRAUS DE LIBERDADE POR NO (SAT) . . . (NGL_S)  = ',I5////)  

200   FORMAT ( '          ECHO DATA: JOBNAME_CONTROL.TXT '           ,///)
      
201   FORMAT ( ' LINHA 1: Xlength, diam, xKref, poro             = ',4f12.5)
202   FORMAT ( ' LINHA 2: rho_w, visco_w, rho_o, visco_o         = ',4f12.5)
203   FORMAT ( ' LINHA 3: ikr, c1w, xkrw_max, c1o, xkro_max      = ',i5,4f12.5)
204   FORMAT ( ' LINHA 4: Qi, Si_inj, Sr_desloc                  = ',e12.5,2f12.5)
205   FORMAT ( ' LINHA 5: Nume  , dt , timef  , nfiles           = ',i8,2f12.5,i5,////)
     
      
      
      
      RETURN
    

      END
