integer FUNCTION CountLines(unit)

implicit none

integer ::  unit, nlines

nlines = 0

DO while (.not. eof(unit))
    READ(unit,*)
    
    nlines = nlines + 1
ENDDO

CountLines = nlines

REWIND(unit)

RETURN

ENDFUNCTION