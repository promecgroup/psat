! *** output for nodal vetor  

      subroutine ensg_vec (tipo,disp,up,id,ngl,numnp,neq,neqp,istep)
	  
	  use modtapes,     only: jobname, iblank, iplt
      
      implicit real*8 (a-h,o-z)
      

      dimension   disp (0:neq), id   (ngl,numnp), up (0:neqp) 
      real*8, allocatable ::  ap(:)
      character*70 filename
      character*1  tipo

!     arquivo Ensight para vetores

      write (filename(1:4),'(i4.4)') istep

      write(filename,'(a)') jobname(1:iblank-1)//'.'//tipo//filename(1:4)

      write(*,'(a)') ' Gravando '//filename(1:50)

      
      open (unit=iplt,file=filename,form='formatted')
      
      write (iplt,'(a,i5,1x,a)') 'Ensight Vector passo ',istep


! ***  

   
   if (neqp.gt.0) then
      
      allocate (ap(-neqp:neq)) ; ap = 0.d0
      
      
         do i = 1, numnp 
            do j = 1, ngl
            
            ieq = id(j,i)
      
	        if (ieq.ge.0) then
	        
	          ap(ieq)=disp(ieq)
            
            else

              ap(ieq)=up(-ieq)

	        endif
          enddo
        enddo

      IF (ngl .eq. 1) THEN
        write (iplt,'(1p,6e12.4E3)') (ap(id(1,i)), &
                                      0.d0, &
                                      0.d0, &
     				                  i=1,numnp)
      
      ELSEIF (ngl .eq. 2) THEN
        write (iplt,'(1p,6e12.4E3)') (ap(id(1,i)), &
                                      ap(id(2,i)), &
                                      0.d0, &
     				                  i=1,numnp)
      ELSEIF (ngl .eq. 3) THEN
        write (iplt,'(1p,6e12.4E3)') (ap(id(1,i)), &
                                      ap(id(2,i)), &
                                      ap(id(3,i)), &
     				                  i=1,numnp)
      ELSE
        write (*,*) 'Error in: Subroutine ensg_vec'
        PAUSE
        STOP
      ENDIF
   
   deallocate (ap)
   
   else
      
      
      IF (ngl .eq. 2) THEN
        write (iplt,'(1p,6e12.4E3)') (disp(id(1,i)), &
                                      disp(id(2,i)), &
                                      0.d0, &
     				                  i=1,numnp)
      ELSEIF (ngl .eq. 3) THEN
        write (iplt,'(1p,6e12.4E3)') (disp(id(1,i)), &
                                      disp(id(2,i)), &
                                      disp(id(3,i)), &
     				                  i=1,numnp)
      ENDIF
   
   endif    
   close (unit=iplt)
      
   return
   end




