      subroutine addban2    (a,maxa,s,lm,nd)
  
!
! **********************************************************************
!  r o t i n a    a d d b a n 2
!
!	para montagem da matriz de rigidez dos elementos na
!	matriz de rigidez global compactada (skyline), triangulo superior
!
!	a matriz de elemento a ser montada deve estar armazenada por 
!	colunas, apenas os termos acima da diagonal inclusive:
!
!	    s1	     s2	     s4	     s7	     s11	s16	    ...
!		     s3	     s5	     s8	     s12	s17	    ...
!			     s6	     s9	     s13	s18	    ...
!				     s10     s14	s19	    ...
!					     s15	s20	    ...
!						        s21	    ...
!							            ...
!
!
!
! **********************************************************************
!

      implicit real*8 (a-h,o-z)        
      dimension a(1),maxa(1),s(1),lm(1)

	do j = 1, nd

		   jeq = lm(j)
		   if (jeq.gt.0) then

		   do i = 1, j

			      ieq = lm(i)
			      if (ieq.gt.0) then

			          is = j*(j-1)/2 + i

			          if (jeq.ge.ieq) then
			              kk = maxa(jeq) + jeq-ieq 
			              
			          else
			              kk = maxa(ieq) + ieq-jeq 

			          endif
			          
			          a(kk) = a(kk) + s(is) 

              

			      endif
			      
		   enddo

		   endif
	
	enddo

     	return
      end




