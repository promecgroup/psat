# Repositório público do projeto PSAT #

Simulação e otimização numérica de ensaio de fluxo unidimensional bi-dásico em meio poroso.

---

## Dados de Entrada ##

Unidades no S.I.

##### jobname.txt #####

Arquivo com o radical do _job_

##### _jobname_\_CONTROL.txt #####

Arquivo com as variáveis do _job_

##### _jobname_\_OPTIM.txt #####

Arquivo com os parâmetros do otimizador do _job_, caso seja uma rodada de otimização.

##### _jobname_\_DISPVOLU.txt #####

Arquivo com a curva experimental de volume produzido por tempo do _job_, para ser usada de objetivo para a otimização.

##### _jobname_\_PRESDROP.txt #####

Arquivo com a curva experimental de queda de pressão por tempo do _job_, para ser usada de objetivo para a otimização.

---

## Pastas

### SOURCES ###

##### ALLOC #####

Subrotinas de alocação de memória para os vetores globais

##### INPUTS #####

Subrotinas de leitura de arquivos de entrada de dados

##### IOMNGR #####

Subrotinas de alocação de leitores e escritores de arquivos

##### MAIN #####

Programa principal

##### MODULES #####

Subrotinas de módulos globais

##### PREP #####

Subrotinas de pré-processamento

##### RUN #####

Subrotinas de simulação e otimização

##### UTILITIES #####

Subrotinas auxiliares

---

### DATA ###

Pasta de exemplos

---

### PROJECT ###

Projeto Visual Studio 10.0