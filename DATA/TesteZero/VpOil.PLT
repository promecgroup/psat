
set key bottom r


set xrange [0:]
set yrange [:]
set grid
set title " Oil Production "
set xlabel "Time (s)"
set ylabel " Cumulative Oil Production (cm3) "


plot   'VpRelp.txt' using 1:($2*1)   title 'Vp_RelP'   with l,\
       'fort.152' every 100 using 1:($2*1e6)   title 'Vp_PSAT'   with l ls 3

pause 1000
