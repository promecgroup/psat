
c1w=3
c1o=3
krw_max=0.5
kro_max=0.5
Swi=0.20
Sro=0.20
Swf=1-Sro

set xrange [:]
set yrange [0:]
set grid
set title " COREY MODEL:  "
set xlabel " Water Saturation "
set ylabel " Kr "


plot     'fort.76' using 1:2   title 'krw' with l,  'fort.76' using 1:3   title 'kro' with l


pause 1000
#set terminal jpeg medium size 800,600
#set output 'KxS.jpg'

