## TITLE
1d Buckley Leverett Problem

## Comprimento       / Diametro    / PERMEABILIDADE KREF / POROSIDADE 
    0.08                0.04           9.86923e-14              0.25  

##  Rho_w            / Visco_w     /     Rho_o           /  Visco_o     
    1000.0              0.001          800.0               0.001

## kr type (Corey=1) /  c1_w       /     Krw_max         /   c1_o      /     Kro_max
      1                  3.0            0.5                 3.0                0.5

##  Qi input 5.56e-8 (m3/s)        / Si Injetada /  Sr Deslocada                       
    5.56e-8                            0.20             0.20     

##  No elem          /   dt        /    Timef            /   NFILES 
    100                  0.5           400.0                 10