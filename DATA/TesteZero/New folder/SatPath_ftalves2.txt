

set xrange [-0.01:0.09]
set yrange [0.0:0.8]
set grid
set title " Saturation Profile   "
set xlabel "x (m) "
set ylabel " Water Saturation "


plot    'fort.201' using 1:2   title 't=P1' with lp,\
 	'case4_satmap.out' using 1:3  title s with l

pause 1000


