

set xrange [0:]
set yrange [:]
set grid
set title " Pressure Profile   "
set xlabel "x (m) "
set ylabel " Pressure (Pa) "


plot    'fort.201' using 1:3   title 't=P1' with l,\
        'fort.202' using 1:3   title 't=P2' with l,\
        'fort.203' using 1:3   title 't=P3' with l,\
        'fort.204' using 1:3   title 't=P4' with l,\
        'fort.205' using 1:3   title 't=P5' with l,\
        'fort.206' using 1:3   title 't=P6' with l,\
        'fort.207' using 1:3   title 't=P7' with l,\
		'case4_presmap.out' using 1:3  title columnheader with p,\
		'case4_presmap.out' using 1:4  title columnheader with p,\
		'case4_presmap.out' using 1:5  title columnheader with p,\
		'case4_presmap.out' using 1:6  title columnheader with p,\
		'case4_presmap.out' using 1:7  title columnheader with p,\
		'case4_presmap.out' using 1:8  title columnheader with p,\
		'case4_presmap.out' using 1:9  title columnheader with p 

pause 1000
