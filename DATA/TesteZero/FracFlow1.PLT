
c1w=3
c1o=3
krw_max=0.5
kro_max=0.5
Swi=0.20
Sro=0.20
Swf=1-Sro

set xrange [:]
set yrange [0:2]
set grid
set title " COREY MODEL: Fractional Flow Function "
set xlabel " Water Saturation "
set ylabel " Fractional Flow Function "


plot     'fort.77' using 1:2   title 'F' with l,  'fort.77' using 1:($3/4)   title 'DF/4' with l


pause 1000
#set terminal jpeg medium size 800,600
#set output 'KxS.jpg'

