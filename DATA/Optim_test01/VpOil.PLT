
set key bottom r


set xrange [0:400]
set yrange [:]
set grid
set title " Oil Production "
set xlabel "Time (s)"
set ylabel " Cumulative Oil Production (cm3) "


plot   'Teste1_dispvolu.out' using 1:($2)   title 'Vp_PSAT'   with p, \
       'VpRelp.txt' using 1:($2)   title 'Vp_RelP'   with l
       

pause 1000
