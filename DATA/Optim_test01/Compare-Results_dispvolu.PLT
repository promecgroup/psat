
set key bottom r


set xrange [0:]
set yrange [:]
set grid
set title " Results "
set xlabel "Time (s)"
set ylabel " Displaced Volume (m3) "


plot   'Teste1_DISPVOLU.txt' using  1:($2)  title 'Target'   with p, \
       'RELP_dispvol.txt' using  1:($2*10**-6)  title 'RELP'   with l, \
       'Teste1_dispvolu.out' using  1:($2)  title 'PSAT'    with l

pause 1000
