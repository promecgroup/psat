

set xrange [0:]
set yrange [:]
set grid
set title " Pressure Profile   "
set xlabel "x (m) "
set ylabel " Pressure (Pa) "


plot    'fort.201' using 1:3   title 't=P1' with lp,\
        'fort.202' using 1:3   title 't=P2' with l,\
        'fort.203' using 1:3   title 't=P3' with l,\
        'fort.204' using 1:3   title 't=P4' with l,\
        'fort.205' using 1:3   title 't=P5' with l,\
        'fort.206' using 1:3   title 't=P6' with l,\
        'fort.207' using 1:3   title 't=P7' with l,\
        'fort.208' using 1:3   title 't=P8' with l,\
        'fort.209' using 1:3   title 't=P9' with l,\
        'fort.210' using 1:3   title 't=TF' with l

pause 1000
