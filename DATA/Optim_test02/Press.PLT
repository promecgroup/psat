


set xrange [0:400]
set yrange [:]
set grid
set title " Time History: Pressure "
set xlabel "Time (s) "
set ylabel " Pressure (Bar) "


plot   'Teste1_presdrop.out'  using 1:($2/1e5)  every 1 title 'PSAT_nfiles'   with p ,\
       'fort.79'  using 1:($2/1e5)  every 1 title 'PSAT_full'   with l ,\
       'PressRelp.txt'  using 1:($2/1e5)  every 1 title 'RelP'   with l


pause 1000
