## TITLE
1d Buckley Leverett Problem

## Phi_tol       / maxIter    / derType
    1e-4           50           1
	
##  delta       / marqInic    / marqMax
    0.001         0.001       1e7
