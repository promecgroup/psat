
set key bottom r


set xrange [0:]
set yrange [:]
set grid
set title " Convergence "
set xlabel "Iteration"
set ylabel " Alpha(1) "


plot   'fort.88' using 1:($3)   title 'f90Only'   with lp, \
       'convergence_cpp.txt' using 1:($3)   title 'cppOnly'   with lp
       

pause 1000
