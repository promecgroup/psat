


set xrange [0:]
set yrange [:]
set grid
set title " Time History: Average Water Saturation "
set xlabel "Time (h) "
set ylabel " Average Water Saturation "


plot   'fort.150' using ($1/3600):2  every 1 title 'el' w l  ,\
       'fort.150' using ($1/3600):3  every 1 title 'xf' w l  

pause 1000
