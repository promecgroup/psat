 _________________________________________________________
        ______________________________________            
                                                          
                      PSAT CODE                           
                                                          
           1d Pressure & Saturation Coupled               
      Finite Element for Porous Media Bi-Phasic Flow      
          Copyright 2018 by PROMEC-RJ-BRAZIL              
                                                          
        ______________________________________            
 _________________________________________________________




                       DADOS  DE CONTROLE                   


 NUMERO DE PONTOS NODAIS . . . . . . . . . . . . (NUMNP)  =       51

 NUMERO DE ELEMENTOS . . . . . . . . . . . . . . (NUME)   =       50

 NUMERO DE NOS POR ELEMENTO  . . . . . . . . . . (NNOEL)  =     2

 NUMERO DE GRAUS DE LIBERDADE POR NO (PRESSAO) . (NGL_P)  =     1

 NUMERO DE GRAUS DE LIBERDADE POR NO (SAT) . . . (NGL_S)  =     1




          ECHO DATA: JOBNAME_CONTROL.TXT 



 LINHA 1: Xlength, diam, xKref, poro             =      0.08000     0.04000     0.00000     0.25000
 LINHA 2: rho_w, visco_w, rho_o, visco_o         =   1000.00000     0.00100   800.00000     0.00100
 LINHA 3: ikr, c1w, xkrw_max, c1o, xkro_max      =     1     2.99000     0.50000     3.01000     0.50000
 LINHA 4: Qi, Si_inj, Sr_desloc                  =  0.55600E-07     0.20000     0.20000
 LINHA 5: Nume  , dt , timef  , nfiles           =       50     0.05000   400.00000  100







 Informacoes do Sistema de Equacoes:
         Numero de Equacoes        (neq) =         50
         Coeficientes Armazenados  (nwk) =         99
         Meia-Banda Maxima         (mk ) =          2
         Meia Banda Media          (mm ) =          1
 
  *** Total Memory Allocated *** (MEGABYTES) =   3.653000000000000E-003
 
